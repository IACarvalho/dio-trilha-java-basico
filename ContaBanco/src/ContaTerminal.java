import java.util.Scanner;

public class ContaTerminal {
    public static void main(String[] args) {
        Integer numero;
        String agencia, nomeCliente;
        Double saldo;

        var input = new Scanner(System.in);

        System.out.println("Entre com o número de sua conta");
        numero = input.nextInt();

        System.out.println("Entre com a agência da conta");
        agencia = input.next();

        System.out.println("Entre com o nome do cliente");
        nomeCliente = input.next();

        System.out.println("Entre com o saldo da conta");
        saldo = input.nextDouble();

        System.out.printf("Olá %s, obrigado por criar uma conta em nosso banco, sua agência é %s, conta %d e seu saldo %.2f já está disponível para saque\n", nomeCliente, agencia, numero, saldo);
    }
}
